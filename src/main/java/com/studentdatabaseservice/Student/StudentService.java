package com.studentdatabaseservice.Student;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StudentService {

    @Autowired
    StudentsRepository studentsRepository;

    public List<Student> getAllStudents(){

        List<StudentEntity> students = new ArrayList<>();
        log.info("Searching for Students...........");
        studentsRepository.findAll().forEach(students::add);
        log.info("Searching for Students Completed");
        return students.stream().map(e ->Student.builder()
                .name(e.getName())
                .standard(e.getStandard())
                .section(e.getSection())
                .roll(e.getRoll())
                .math(e.getMath())
                .english(e.getEnglish())
                .science(e.getScience())
                .total(e.getTotal())
                .grade(e.getGrade())
                .ranking(e.getRanking())
                .build()).collect(Collectors.toList());
    }

    public List<Student> insertNewStudentsDetails(List<Student> studentList){

        log.info("Inserting Students records to Data Base........");
        List<StudentEntity> savedStudentsList = studentList.stream().map(e -> studentsRepository.save(StudentEntity.builder()
                .name(e.getName())
                .standard(e.getStandard())
                .section(e.getSection())
                .roll(e.getRoll())
                .math(e.getMath())
                .english(e.getEnglish())
                .science(e.getScience())
                .total(e.getTotal())
                .grade(e.getGrade())
                .ranking(e.getRanking())
                .build())).collect(Collectors.toList());

        log.info("Inserting Students records to Data Base Completed");
        return savedStudentsList.stream().map(e ->Student.builder()
                .name(e.getName())
                .standard(e.getStandard())
                .section(e.getSection())
                .roll(e.getRoll())
                .math(e.getMath())
                .english(e.getEnglish())
                .science(e.getScience())
                .total(e.getTotal())
                .grade(e.getGrade())
                .ranking(e.getRanking())
                .build()).collect(Collectors.toList());
    }
}
