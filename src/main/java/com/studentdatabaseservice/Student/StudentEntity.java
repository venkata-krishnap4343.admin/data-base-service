package com.studentdatabaseservice.Student;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name ="students")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentEntity {

    @Id
    private long Id;
    private String name;
    private String standard;
    private String section;
    private String roll;
    private Double math;
    private Double english;
    private Double science;
    private Double total;
    private String grade;
    private String ranking;
}
