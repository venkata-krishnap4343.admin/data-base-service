package com.studentdatabaseservice.Student;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "v1/students/")
@Slf4j
public class StudentController {

    @Autowired
    StudentService studentService;

    public void getStudentsDetailWithNames(){

    }

    public void getStudentsDetailWithRolls(){

    }

    public void getStudentsDetailWithSections(){

    }

    public void getStudentsDetailWithGrades(){

    }

    @GetMapping(path = "/getall")
    public List<Student> getAllStudents(){
        log.info("Request for all Student details at : {}", new Date());
        List<Student> response = studentService.getAllStudents();
        log.info("Request for all Student details at : {}", new Date());
        return response;
    }

    @PostMapping(path = "/addstudentsdetails")
    public void addStudentsDetails(@RequestBody List<Student> studentList){
        log.info("Request to add new student details at: {} ", new Date());
        List<Student> response = studentService.insertNewStudentsDetails(studentList);
        log.info("Request to add new student details completed at: {} ", new Date());
    }
}
