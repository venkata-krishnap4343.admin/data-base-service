package com.studentdatabaseservice.Student;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    private String name;
    private String standard;
    private String section;
    private String roll;
    private Double math;
    private Double english;
    private Double science;
    private Double total;
    private String grade;
    private String ranking;
}
